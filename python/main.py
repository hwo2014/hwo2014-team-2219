import json
import socket
import sys
import copy


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.oldAngle = 0
        self.oldDAngle = 0
        self.oldDistance = 0
        self.oldThrottle = 0
        self.lengths = []
        self.bends = []
        self.turbos = []
        self.averageV = []
        self.maxV = []
        self.maxA = []
        self.countPiece = 0
        self.totalDistance = 0
        self.hasSwitch = []
        self.shouldSwitch = []
        self.angles = []
        self.radius = []
        self.hasTurbo = False
        self.hasCrash = False
        self.oldPieceIndex = -1
        self.oldInPieceDistance = 0
        self.oldVelocity = 0
        self.currentLap = 0
        self.totalLaps = 0
        self.raceFinished = False
        self.gameTick = 0

    def msg_tick(self, msg_type, data, tick):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": tick}))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def joinRace(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                           "key": self.key},
                                     # "trackName": "france",
                                     # "trackName": "germany",
                                     # "trackName": "usa",
                                     # "trackName": "suzuka",
                                     # "trackName": "england",
                                     "trackName": "imola",
                                     "carCount": 1})

    def throttle(self, throttle):
        # print ("Throttle at " + str(self.gameTick))
        if self.gameTick >= 0:
            self.msg_tick("throttle", throttle, self.gameTick)
        else:
            self.ping()
        # self.msg("throttle", throttle)

    def switchLane(self, direction):
        print ("Change to " + direction)
        self.msg_tick("switchLane", direction, self.gameTick)
        # self.msg("switchLane", direction)

    def turbo(self):
        self.msg_tick("turbo", "Pow pow pow pow pow, or your of personalized turbo message", self.gameTick)
        # self.msg("turbo", "Pow pow pow pow pow, or your of personalized turbo message")
        print ("Pow pow pow pow pow " + str(self.gameTick))

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        # self.joinRace()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Race init")
        # print(data)
        try:
            self.quickRace = data['race']['raceSession']['quickRace']
        except:
            self.quickRace = True
        try:
            self.totalLaps = data['race']['raceSession']['laps']
        except:
            self.totalLaps = 1
        self.currentLap = 0
        
        if len(self.lengths) <= 0:
            for piece in data['race']['track']['pieces']:
                try:
                    self.lengths.append(piece['length'])
                    self.bends.append(False)
                except KeyError:
                    self.lengths.append((abs(piece['angle']) * 3.14 * (piece['radius'] + 10)) / 180)
                    self.bends.append(True)
                try:
                    if piece['switch']:
                        self.hasSwitch.append(True)
                except KeyError:
                    self.hasSwitch.append(False)
                try:
                    self.angles.append(piece['angle'])
                    self.radius.append(piece['radius'])
                except KeyError:
                    self.angles.append(0)
                    self.radius.append(0)
            
            for i in range(0, len(self.bends)):
                if self.bends[i] == True:
                    next1 = (i + 1) % len(self.bends)
                    next2 = (i + 2) % len(self.bends)
                    next3 = (i + 3) % len(self.bends)
                    if self.bends[next1] == False and self.bends[next2] == False and self.bends[next3] == False:
                        self.turbos.append(True)
                    else:
                        self.turbos.append(False)
                else:
                    self.turbos.append(False)
        
        self.averageV = []
        self.maxA = []
        for i in range(0, len(self.bends)):
            temp1 = [0] * (self.totalLaps + 1)
            temp2 = [0] * (self.totalLaps + 1)
            self.averageV.append(temp1)
            self.maxA.append(temp2)
        
        if len(self.shouldSwitch) <= 0:
            i = 0
            self.shouldSwitch = ['none'] * len(self.bends)
            while i < len(self.bends):
                if self.hasSwitch[i] == True:
                    j = i
                    angle = 0
                    while self.bends[j] == False:
                        j = (j + 1) % len(self.bends)
                    while self.bends[j] == True:
                        angle += self.angles[j]
                        j = (j + 1) % len(self.bends)
                    # print (angle)
                    if angle > 0:
                        self.shouldSwitch[i - 1] = 'right'
                    else:
                        self.shouldSwitch[i - 1] = 'left'
                i = i + 1
        
        if len(self.maxV) <= 0:
            self.maxV = [0] * len(self.bends)
            i = 0
            while i < len(self.bends):
                if self.bends[i] == True:
                    if self.radius[i] >= 100:
                        self.maxV[i] = 8
                    else:
                        self.maxV[i] = 7
                    k = (i + 1) % len(self.bends)
                    angle = abs(self.angles[i])
                    while self.bends[k] == True:
                        self.maxV[k] = self.maxV[k - 1] - 1
                        angle += abs(self.angles[k])
                        k = (k + 1) % len(self.bends)
                    
                    if self.radius[i] >= 100:
                        if abs(angle) >= 179.0: # 45 + 45 + 45 + 45
                            minV = 5.0
                        elif abs(angle) >= 134.0: # 45 + 45 + 45
                            minV = 5.5
                        elif abs(angle) >= 111.0: # 45 + 45 + 22.5
                            minV = 6.0
                        elif abs(angle) >= 89.0: # 45 + 45
                            minV = 6.5
                        elif abs(angle) >= 66.0: # 45 + 22.5
                            minV = 7.0
                        elif abs(angle) >= 44.0: # 45
                            minV = 7.5
                        else:
                            minV = 8.0
                    else:
                        if abs(angle) >= 179.0: # 45 + 45 + 45 + 45
                            minV = 3.5
                        elif abs(angle) >= 134.0: # 45 + 45 + 45
                            minV = 4.0
                        elif abs(angle) >= 111.0: # 45 + 45 + 22.5
                            minV = 4.5
                        elif abs(angle) >= 89.0: # 45 + 45
                            minV = 5.0
                        elif abs(angle) >= 66.0: # 45 + 22.5
                            minV = 5.5
                        elif abs(angle) >= 44.0: # 45
                            minV = 6.0
                        else:
                            minV = 7.0
                    
                    j = (i - 1) % len(self.bends)
                    if self.bends[j] == False:
                        # print (minV)
                        self.maxV[j] = minV
                        while True:
                            tempV = self.maxV[j]
                            j = (j - 1) % len(self.bends)
                            if self.bends[j] == False:
                                self.maxV[j] = tempV + 1.0
                            else:
                                break
                    i = k + 1
                else:
                    i = i + 1
        
        print ('+----+------+--------+-------+--------+--------+--------+-------+')
        print chr(179),'{0:2}'.format('No'),
        print chr(179),'{0:4}'.format('Type'),
        print chr(179),'{0:6}'.format('Length'),
        print chr(179),'{0:5}'.format('Angle'),
        print chr(179),'{0:6}'.format('Radius'),
        print chr(179),'{0:6}'.format('Switch'),
        print chr(179),'{0:6}'.format('MaxV'),
        print chr(179),'{0:5}'.format('Turbo'),chr(179)
        print ('+----+------+--------+-------+--------+--------+--------+-------+')
        for i in range(0, len(self.bends)):
            print chr(179),'{0:2}'.format(str(i + 1)),
            if self.bends[i]:
                type = 'b'
                print chr(179),'{0:4}'.format(chr(239)),
                print chr(179),'{0:6}'.format(''),
                print chr(179),'{0:5}'.format(self.angles[i]),
                print chr(179),'{0:6}'.format(self.radius[i]),
            else:
                type = 's'
                print chr(179),'{0:4}'.format(chr(205)),
                print chr(179),'{0:6}'.format(self.lengths[i]),
                print chr(179),'{0:5}'.format(''),
                print chr(179),'{0:6}'.format(''),
            
            if self.hasSwitch[i]:
                print chr(179),'{0:6}'.format('Yes'),
            else:
                print chr(179),'{0:6}'.format('No'),
            print chr(179),'{0:6.1f}'.format(self.maxV[i]),
            
            if self.turbos[i]:
                print chr(179),'{0:5}'.format('Yes'),chr(179)
            else:
                print chr(179),'{0:5}'.format(''),chr(179)
            print ('+----+------+--------+-------+--------+--------+--------+-------+')
        
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)

    def on_car_positions(self, data):
        if self.gameTick < 0:
            return
        if self.hasCrash:
            self.ping()
            return
        for racer in data:
            if racer['id']['name'] == 'vmoga':
                me = racer
        angle = me['angle']
        pieceIndex = me['piecePosition']['pieceIndex']
        inPieceDistance = me['piecePosition']['inPieceDistance']
        
        if self.maxA[pieceIndex][self.currentLap] < abs(angle):
            self.maxA[pieceIndex][self.currentLap] = abs(angle)
        
        changeLane = False
        if self.oldPieceIndex < 0:
            self.oldPieceIndex = pieceIndex
        if self.oldPieceIndex == pieceIndex:
            velocity = inPieceDistance - self.oldInPieceDistance
            self.averageV[pieceIndex][self.currentLap] += velocity
            self.countPiece += 1
        else:
            changeLane = True
            self.averageV[self.oldPieceIndex][self.currentLap] /= self.countPiece
            # print ("Average V: " + str(self.averageV[self.oldPieceIndex][self.currentLap]))
            if self.raceFinished == True:
                self.ping()
                return
            if self.totalLaps > 1:
                if pieceIndex == 0:
                    self.currentLap += 1
                    if self.currentLap > self.totalLaps:
                        self.currentLap = self.totalLaps
            self.countPiece = 1
            self.oldPieceIndex = pieceIndex
            velocity = self.oldVelocity
            self.averageV[pieceIndex][self.currentLap] += velocity
        self.oldInPieceDistance = inPieceDistance
        self.oldVelocity = velocity
        
        dAngle = angle - self.oldAngle
        self.oldAngle = angle
        
        newThrottle = 0.0
        nextPiece = (pieceIndex + 1) % len(self.bends)
        
        if changeLane and self.shouldSwitch[pieceIndex] == 'left':
            self.switchLane("Left")
            return
        elif changeLane and self.shouldSwitch[pieceIndex] == 'right':
            self.switchLane("Right")
            return
        
        if self.hasTurbo == True and self.turbos[pieceIndex] == True:
            self.turbo()
            self.hasTurbo = False
            return
        
        if self.bends[pieceIndex] == False:
            if abs(angle) > 30.0:
                newThrottle = 0.0
            elif abs(angle) > 20.0:
                newThrottle = 0.1
            elif self.maxV[pieceIndex] < velocity:
                newThrottle = 0.0
            else:
                newThrottle = 1.0
        else:
            if abs(angle) > 35.0:
                newThrottle = 0.0
            elif abs(angle) > 30.0:
                newThrottle = 0.1
            elif abs(angle) > 20.0:
                newThrottle = 0.2
            elif abs(angle) > 10.0:
                if self.maxV[pieceIndex] < velocity:
                    newThrottle = 0.1
                else:
                    newThrottle = 0.35
            elif abs(angle) > 5.0:
                if self.maxV[pieceIndex] < velocity:
                    newThrottle = 0.35
                else:
                    newThrottle = 0.7
            else:
                if self.maxV[pieceIndex] < velocity:
                    newThrottle = 0.8
                else:
                    newThrottle = 1.0
        
        self.throttle(newThrottle)
        
        if self.bends[pieceIndex]:
            type = 'b'
        else:
            type = 's'
        print ('+----+------+------+--------+--------+--------+-------+-----------')
        print chr(179),'{0:2}'.format('No'),
        print chr(179),'{0:4}'.format('newT'),
        print chr(179),'{0:4}'.format('oldT'),
        print chr(179),'{0:6}'.format('dAngle'),
        print chr(179),'{0:6}'.format('angle'),
        print chr(179),'{0:6}'.format('dist'),
        print chr(179),'{0:5}'.format('V'),
        print chr(179),'{0:10}'.format('tic')
        print ('+----+------+------+--------+--------+--------+-------+-----------')
        print chr(179),'{0:2}'.format(str(pieceIndex + 1)),
        print chr(179),'{0:3.2f}'.format(self.oldThrottle),
        print chr(179),'{0:3.2f}'.format(newThrottle),
        print chr(179),'{0:6.2f}'.format(dAngle),
        print chr(179),'{0:6.2f}'.format(angle),
        print chr(179),'{0:6.2f}'.format(inPieceDistance),
        print chr(179),'{0:5.2f}'.format(velocity),
        print chr(179),'{0:10d}'.format(self.gameTick)
        # print ('+----+------+------+--------+--------+--------+-------+-----------')
        print ('')
        self.oldThrottle = newThrottle
        self.oldDAngle = dAngle

    def on_turbo_available(self, data):
        print("Got turbo")
        if self.hasCrash == True:
            print("Crashing")
        else:
            self.hasTurbo = True
        self.ping()

    def on_crash(self, data):
        print(data['name'] + " crashed")
        if data['name'] == 'vmoga':
            self.hasCrash = True
        self.ping()

    def on_spawn(self, data):
        print(data['name'] + " spawned")
        if data['name'] == 'vmoga':
            self.hasCrash = False
        self.ping()

    def on_lap_finished(self, data):
        print(str(data['car']['name']) + " finished lap " + str(data['lapTime']['lap']) + " in " + str(data['lapTime']['millis']) + " at tic #" + str(data['lapTime']['ticks']))
        if data['car']['name'] == 'vmoga':
            print ('+----+------+------+------+------+------+------+------+------+------+------')
            print chr(179),'{0:2}'.format('No'),
            for i in range(0, self.totalLaps):
                print chr(179),'{0:4}'.format('Lap' + str(i + 1)),
            print ('')
            print ('+----+------+------+------+------+------+------+------+------+------+------')
            for i in range(0, len(self.averageV)):
                if self.bends[i]:
                    type = 'b'
                else:
                    type = 's'
                print chr(179),'{0:2}'.format(str(i + 1)),
                for j in range(0, self.totalLaps):
                    print chr(179),'{0:4.1f}'.format(self.maxA[i][j]),
                print ('')
                print ('+----+------+------+------+------+------+------+------+------+------+------')
            
            if self.currentLap < self.totalLaps:
                i = 0
                stop = False
                oldMaxV = copy.deepcopy(self.maxV)
                while i < len(self.angles):
                    angle = self.maxA[i][self.currentLap - 1]
                    k = i + 1
                    while True:
                        if k >= len(self.angles):
                            stop = True
                            break;
                        if self.maxA[k][self.currentLap - 1] > angle:
                            break
                        else:
                            angle = self.maxA[k][self.currentLap - 1]
                        k = k + 1
                    
                    while True and stop == False:
                        k = k + 1
                        if k < len(self.angles) and self.maxA[k][self.currentLap - 1] > angle:
                            angle = self.maxA[k][self.currentLap - 1]
                        else:
                            break
                    
                    if (angle < 20) and stop == False:
                        i = k - 1
                        print (k)
                        while self.bends[i] == True:
                            self.maxV[i] += 0.3
                            i = (i - 1) % len(self.angles)
                        while self.bends[i] == False:
                            self.maxV[i] += 0.8
                            i = (i - 1) % len(self.angles)
                    elif angle < 30 and stop == False:
                        i = k - 1
                        print (k)
                        while self.bends[i] == True:
                            self.maxV[i] += 0.2
                            i = (i - 1) % len(self.angles)
                        while self.bends[i] == False:
                            self.maxV[i] += 0.5
                            i = (i - 1) % len(self.angles)
                    elif angle < 35 and stop == False:
                        i = k - 1
                        print (k)
                        while self.bends[i] == True:
                            self.maxV[i] += 0.1
                            i = (i - 1) % len(self.angles)
                        while self.bends[i] == False:
                            self.maxV[i] += 0.25
                            i = (i - 1) % len(self.angles)
                    elif angle > 55 and stop == False:
                        i = k - 1
                        print (k)
                        while self.bends[i] == True:
                            self.maxV[i] -= 0.2
                            i = (i - 1) % len(self.angles)
                        while self.bends[i] == False:
                            self.maxV[i] -= 0.5
                            i = (i - 1) % len(self.angles)
                    i = k
                print ('+----+------+--------+--------+--------+')
                print chr(179),'{0:2}'.format('No'),
                print chr(179),'{0:4}'.format('Type'),
                print chr(179),'{0:6}'.format('OldV'),
                print chr(179),'{0:6}'.format('MaxV'),
                print chr(179),'{0:6}'.format('AvgV'),chr(179)
                print ('+----+------+--------+--------+--------+')
                for i in range(0, len(self.maxV)):
                    print chr(179),'{0:2}'.format(str(i + 1)),
                    if self.bends[i]:
                        print chr(179),'{0:4}'.format(chr(239)),
                    else:
                        print chr(179),'{0:4}'.format(chr(205)),
                    print chr(179),'{0:6.1f}'.format(oldMaxV[i]),
                    print chr(179),'{0:6.1f}'.format(self.maxV[i]),
                    print chr(179),'{0:6.1f}'.format(self.averageV[i][self.currentLap - 1]),chr(179)
                    print ('+----+------+--------+--------+--------+')
        self.ping()

    def on_finish(self, data):
        self.raceFinished = True
        print(str(data['name']) + " finished.")
        if data['name'] == 'vmoga':
            for i in range(0, len(self.averageV)):
                if self.bends[i]:
                    type = 'b'
                else:
                    type = 's'
                print '{0:3}'.format(str(i) + type),
                for j in range(0, self.totalLaps):
                    print '{0:17}'.format(str(self.maxA[i][j])),
                print ("")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            try:
                if msg['msgType'] == 'carPositions' or msg['msgType'] == 'gameStart':
                    self.gameTick = msg['gameTick']
            except:
                self.gameTick = -1
            # print (msg_type, ' ', self.gameTick)
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
